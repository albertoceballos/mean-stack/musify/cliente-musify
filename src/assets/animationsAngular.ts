import { trigger, state, style, animate, transition } from '@angular/animations';

export var fadeInOut = trigger('Fading', [
    state('void', style({ opacity: 0 })),
    state('*', style({ opacity: 1 })),
    transition(':enter', animate('800ms ease-out')),
    transition(':leave', animate('800ms ease-in')),
]);