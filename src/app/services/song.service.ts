import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { UserService } from './user.service';


//servicios

@Injectable({
  providedIn: 'root'
})
export class SongService {

  public url: string;
  constructor(private _httpclient: HttpClient, private _userService: UserService) {
    this.url = environment.apiURL + 'song/';
  }

  //crear nueva canción
  create(song): Observable<any> {
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });
    return this._httpclient.post(this.url + 'create', song, { headers });
  }

  //extraer canción por su Id
  getSong(songId): Observable<any> {
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });
    return this._httpclient.get(this.url + 'get/' + songId, { headers });
  }

  //actualizar canción
  update(songId, song): Observable<any> {
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });
    return this._httpclient.put(this.url + 'update/' + songId, song, { headers });
  }

  //lisatado de canciones
  getAll(albumId = null):Observable<any> {
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });
    if (albumId != null) {
      //si hay parámetro de id de album, extraer las canciones de ese álbum
      return this._httpclient.get(this.url + 'getAll/' + albumId, { headers });

    } else {
      //si el parámetro de id de album es nulo, sacar todas las canciones de la aplicación
      return this._httpclient.get(this.url + 'getAll', { headers });
    }
  }

  //borrar canción
  delete(songId):Observable<any>{
    var token = this._userService.getToken();
    var headers = new HttpHeaders({ 'Authorization': token });
    return this._httpclient.delete(this.url +'delete/' + songId, {headers});
  }
}
