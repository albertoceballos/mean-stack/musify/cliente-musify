import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../models/User';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  public URL: string;
  public identity: User;
  public token: string;
  constructor(private _httpClient: HttpClient) {
    this.URL = environment.apiURL;
  }

  getIdentity() {
    let identity = JSON.parse(localStorage.getItem('identity'));
    if (identity != 'undefined') {
      this.identity = identity;
    } else {
      this.identity = null;
    }
    return this.identity;
  }

  getToken() {
    let token = localStorage.getItem('token');
    if (token != 'undefined') {
      this.token = token;
    } else {
      this.token = null;
    }
    return this.token;
  }

  login(loginData): Observable<any> {
    return this._httpClient.post(this.URL + 'user/login', loginData);
  }

  register(user): Observable<any> {
    return this._httpClient.post(this.URL + 'user/create', user);
  }

  update(user): Observable<any> {
    var headers=new HttpHeaders({'Authorization':this.token});
    return this._httpClient.put(this.URL + 'user/update', user,{headers});
  }
}
