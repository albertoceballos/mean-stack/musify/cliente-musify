import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router ,CanActivate } from '@angular/router';

import { UserService } from './user.service';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class IdentityGuard implements CanActivate {
  
  public identity:User;
  constructor(private _userService:UserService, private _router:Router){

  }
  canActivate(){
    this.identity=this._userService.getIdentity();
    if(this.identity && this.identity.role=='ROLE_ADMIN'){
      return true;
    }else{
      this._router.navigate(['/login']);
      return false;
    }
  }
}
