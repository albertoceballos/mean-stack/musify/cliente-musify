export class Artist{
    public _id:string;
    public name:string;
    public description:string;
    public image:string;

    constructor(_id,name,description,image){
        this._id=_id;
        this.name=name;
        this.description=description;
        this.image=image;
    }
}