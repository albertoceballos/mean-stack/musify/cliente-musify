import { Component, OnInit, DoCheck } from '@angular/core';
import { User } from './models/User';
import { UserService } from './services/user.service';
import {Router} from '@angular/router';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent implements OnInit, DoCheck {
  public title = 'Musify';
  public identity:User;
  public token:string;
  public url:string;
  public searchString:string;
  constructor(private _userService:UserService,private _router:Router){
    this.url=environment.apiURL;
  }

  ngOnInit(){
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
  }

  ngDoCheck(){
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
  }

  logout(){
    this.identity=null;
    this.token=null;
    localStorage.clear();
    this._router.navigate(['/login']);
    
  }

  onSubmitSearch(form){
    this._router.navigate(['/search/'+this.searchString]);
  }

}
