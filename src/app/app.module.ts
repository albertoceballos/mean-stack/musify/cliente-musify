import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { ArtistListComponent } from './components/artist-list/artist-list.component';
import { ArtistAddComponent } from './components/artist-add/artist-add.component';
import { ArtistEditComponent } from './components/artist-edit/artist-edit.component';

//guard
import { IdentityGuard } from './services/identity.guard';
import { ArtistDetailComponent } from './components/artist-detail/artist-detail.component';
import { AlbumAddComponent } from './components/album-add/album-add.component';
import { AlbumEditComponent } from './components/album-edit/album-edit.component';
import { AlbumDetailComponent } from './components/album-detail/album-detail.component';
import { SongAddComponent } from './components/song-add/song-add.component';
import { SongEditComponent } from './components/song-edit/song-edit.component';
import { PlayerComponent } from './components/player/player.component';
import { AlbumListComponent } from './components/album-list/album-list.component';
import { SearchComponent } from './components/search/search.component';

//Rutas
const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent },
  { path: 'user-edit', component: UserEditComponent },
  //artistas
  { path: 'artists', component: ArtistListComponent },
  { path: 'artist-add', component: ArtistAddComponent, canActivate: [IdentityGuard] },
  { path: 'artist-edit/:artistId', component: ArtistEditComponent, canActivate: [IdentityGuard] },
  { path: 'artist/:artistId', component: ArtistDetailComponent },
  //albums
  { path: 'album-add/:artistId', component: AlbumAddComponent, canActivate: [IdentityGuard] },
  { path: 'album-edit/:albumId', component: AlbumEditComponent, canActivate: [IdentityGuard] },
  { path: 'album/:albumId', component: AlbumDetailComponent },
  { path: 'albums', component: AlbumListComponent },
  //Canciones
  { path: 'song-add/:albumId', component: SongAddComponent, canActivate: [IdentityGuard] },
  { path: 'song-edit/:songId', component: SongEditComponent, canActivate: [IdentityGuard] },
  //búsqueda
  { path: 'search/:searchString',component: SearchComponent},
  //ruta principal
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  
  
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserEditComponent,
    ArtistListComponent,
    ArtistAddComponent,
    ArtistEditComponent,
    ArtistDetailComponent,
    AlbumAddComponent,
    AlbumEditComponent,
    AlbumDetailComponent,
    SongAddComponent,
    SongEditComponent,
    PlayerComponent,
    AlbumListComponent,
    SearchComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
