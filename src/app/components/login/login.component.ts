import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { UserService } from '../../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

//archivo animaciones
import {fadeInOut} from '../../../assets/animationsAngular';
import { animate } from '@angular/animations';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  animations:[fadeInOut],
})
export class LoginComponent implements OnInit {

  public user: User;
  public identity: User;
  public status: boolean;
  public message: string;
  public token: string;
  constructor(private _userService: UserService, private _router:Router) {
    this.user = new User('', 'ROLE_USER', '', '', '', '', '', false);
  }

  ngOnInit() {

  }

  onSubmit() {
    console.log(this.user);
    //comprobar usuario
    this._userService.login(this.user).subscribe(
      response => {
        this.status = response.ok;
        if (this.status == true) {
          this.identity = response.user;
          this.message = response.message;
          //conseguir token
          this.user.getHash = true;
          this._userService.login(this.user).subscribe(
            resp => {
              if (resp.ok = true) {
                this.token = resp.token;
                delete this.identity.password;
                console.log(this.identity);
                //persistir usuario y token en el localStorage
                localStorage.setItem('identity', JSON.stringify(this.identity));
                localStorage.setItem('token', this.token);
                //redirigir a la home
                setTimeout(() => {
                  this._router.navigate(['/home']);
                },1500);
              }
            }
          );
        }
      },
      error => {
        console.log(error);
        this.status = false;
        if (error.error.message) {
          this.message = error.error.message;
        } else {
          this.message = "Error en el servidor";
        }
      });
  }

}
