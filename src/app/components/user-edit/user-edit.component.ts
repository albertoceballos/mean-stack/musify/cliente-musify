import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import {fadeInOut} from '../../../assets/animationsAngular';
import { UserService } from 'src/app/services/user.service';

import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-user-edit',
  templateUrl:'./user-edit.component.html' ,
  styleUrls: ['./user-edit.component.css'],
  animations:[fadeInOut],
})
export class UserEditComponent implements OnInit {

  public title:string;
  public user:User;
  public status:boolean;
  public identity:User;
  public message:string
  public token:string;
  public filesToUpload:Array<File>;
  public url:string;
  constructor(private _userService:UserService) {
    this.title="Editar Usuario";
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.user=this.identity;
    this.url=environment.apiURL;
   }

  ngOnInit() {
    console.log(this.user);
  }

  onSubmit(){
    this._userService.update(this.user).subscribe(
      response=>{
        this.status=true;
        console.log(response);
        this.identity=response.user;
        localStorage.setItem('identity',JSON.stringify(this.identity));
        if(this.filesToUpload){
          this.UploadFileRequest(this.url+'user/upload-avatar',[],this.filesToUpload).then((result:any)=>{
            this.identity.image=result.image;
            localStorage.setItem('identity',JSON.stringify(this.identity));
          });
        }
        this.message=response.message;
        console.log(this.identity);
      },
      error=>{
        this.status=false;
        if(error.error.message){
          this.message=error.error.message;
        }else{
          this.message="Error al actualizar";
        }
      }
    );
  }

  //Evento que al seleccionar imagen recoge la imagen
  fileChangeEvent(fileInput:any){
    this.filesToUpload=fileInput.target.files;
    console.log(this.filesToUpload);
  }

  //subida de imagen mediante petición Ajax
  UploadFileRequest(url:string,params:Array<any>,files:Array<File>){
    var token=this.token;
    return new Promise((resolve,reject)=>{
      var formData=new FormData();
      var xhr=new XMLHttpRequest();
      for(let i=0;i<files.length;i++){
        formData.append('imagen',files[i],files[i].name);
      }

      xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
          if(xhr.status==200){
            resolve(JSON.parse(xhr.response));
          }else{
            reject(xhr.response);
          }
        }
      }
      xhr.open('POST',url,true);
      xhr.setRequestHeader('Authorization',token);
      xhr.send(formData);
      
    });
  }

}
