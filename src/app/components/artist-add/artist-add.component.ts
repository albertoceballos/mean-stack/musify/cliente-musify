import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
//modelos
import { Artist } from 'src/app/models/Artist';
//servicios
import { ArtistService } from 'src/app/services/artist.service';
//animaciones angular
import {fadeInOut} from '../../../assets/animationsAngular';

@Component({
  selector: 'app-artist-add',
  templateUrl: './artist-add.component.html',
  styleUrls: ['./artist-add.component.css'],
  animations:[fadeInOut],
})
export class ArtistAddComponent implements OnInit {

  public title:string;
  public artist:Artist;
  public status:boolean;
  public message:string;
  public is_edit:boolean;
  constructor(private _artistService:ArtistService,private _router:Router) {
    this.title="Añadir artista";
    this.is_edit=false;
    this.artist=new Artist('','','',null);
   }

  ngOnInit() {
  }

  onSubmit(){
    this._artistService.create(this.artist).subscribe(
      response=>{
        console.log(response);
        this.status=true;
        this.message=response.message;
        var artistId=response.artist._id;
        setTimeout(()=>{
          this._router.navigate(['/artist-edit/'+artistId]);
        },1500);
      },
      error=>{
        console.log(error);
        this.status=false;
        if(error.error.message){
          this.message=error.error.message;
        }else{
          this.message="Error al crear artista, inténtalo más tarde";
        }
        
      }
    );
  }

}
