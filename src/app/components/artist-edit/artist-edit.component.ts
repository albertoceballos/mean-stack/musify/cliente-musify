import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Artist } from 'src/app/models/Artist';
import { fadeInOut } from '../../../assets/animationsAngular';
import { environment } from '../../../environments/environment';
//servicios
import { ArtistService } from 'src/app/services/artist.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-artist-edit',
  templateUrl: '../artist-add/artist-add.component.html',
  styleUrls: ['./artist-edit.component.css'],
  animations: [fadeInOut],
})
export class ArtistEditComponent implements OnInit {

  public title: string;
  public artist: Artist;
  public is_edit: boolean;
  public url: string;
  public fileToUpload: Array<File>;
  public token: string;
  public artistId: string;
  public status: boolean;
  public message: string;
  constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _artistService: ArtistService, private _userService: UserService) {
    this.title = 'Editar artista';
    this.is_edit = true;
    this.url = environment.apiURL;
    this.token = this._userService.getToken();
  }

  ngOnInit() {
    //extraer el id de artista de la url
    this._activatedRoute.params.subscribe(
      params => {
        var artistId = params['artistId'];
        this.artistId = artistId;
        //conseguir datos del artista
        this._artistService.getArtist(this.artistId).subscribe(
          response => {
            this.artist = response.artist;
            console.log(this.artist);
          },
          error => {
            console.log(error);
          }
        );
      }
    );
  }


  onSubmit() {
    this._artistService.update(this.artist, this.artistId).subscribe(
      response => {
        console.log(response);
        if (this.fileToUpload) {
          this.UploadFileRequest(this.url + 'artist/upload-image/' + this.artistId, [], this.fileToUpload).then((result: any) => {
            console.log("Resultado subida imagen: ", result);
            //actualizar imagen mostrada: 
            document.getElementById('image').setAttribute('src', this.url + 'artist/get-image/' + this.artist._id);
          }).catch((err) => {
            console.log(JSON.parse(err));
            var errorMessage = JSON.parse(err);
            this.status = false;
            this.message = errorMessage.message;
            return;
          });
        }
        this.status = true;
        this.message = response.message;
        setTimeout(()=>{
          this._router.navigate(['/artist/'+this.artist._id]);
        },1500);
      },
      error => {
        console.log(error);
        this.status = false;
        if (error.error.message) {
          this.message = error.error.message;
        } else {
          this.message = "Error al actualizar artista, inténtalo más tarde";
        }
      }
    );
  }

  onChangeImage(evento) {
    this.fileToUpload = evento.target.files;
    console.log(this.fileToUpload);
  }

  //subida de imagen mediante petición Ajax
  UploadFileRequest(url: string, params: Array<any>, files: Array<File>) {
    var token = this.token;
    return new Promise((resolve, reject) => {
      var formData = new FormData();
      var xhr = new XMLHttpRequest();
      for (let i = 0; i < files.length; i++) {
        formData.append('image', files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open('POST', url, true);
      xhr.setRequestHeader('Authorization', token);
      xhr.send(formData);

    });
  }

}
