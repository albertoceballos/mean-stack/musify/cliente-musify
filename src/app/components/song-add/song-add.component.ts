import { Component, OnInit } from '@angular/core';
import { fadeInOut } from 'src/assets/animationsAngular';
import { ActivatedRoute, Router } from '@angular/router';

//modelos
import {Song} from 'src/app/models/Song';
import { Album } from 'src/app/models/Album';
//servicios
import { AlbumService } from 'src/app/services/album.service';
import { SongService } from 'src/app/services/song.service';


@Component({
  selector: 'app-song-add',
  templateUrl: './song-add.component.html',
  styleUrls: ['./song-add.component.css'],
  animations:[fadeInOut],
})
export class SongAddComponent implements OnInit {

  public pageTitle:string;
  public album:Album;
  public song:Song;
  public status:boolean;
  public message:string;
  public buttonLabel:string;
  public is_edit:boolean;
  constructor(private _activatedRoute:ActivatedRoute, private _router:Router,private _albumService:AlbumService, private _songService:SongService) {
    this.pageTitle="Añadir canción al album: ";
    this.buttonLabel="Crear canción";
    this.is_edit=false;
    this.song=new Song('','','','3:00',null,'');    
   }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params=>{
        var albumId=params['albumId'];
        this.getAlbum(albumId);
      }
    );
  }

  getAlbum(albumId){
    this._albumService.get(albumId).subscribe(
      response=>{
        this.album=response.album;
        this.song.album=this.album._id;
      },
      error=>{
        console.log(error);
      }
    );
  }

  onSubmit(){
    console.log(this.song);
    this._songService.create(this.song).subscribe(
      response=>{
        console.log(response);
        this.status=true;
        this.song=response.song;
        this.message=response.message;
        setTimeout(()=>{
          this._router.navigate(['/song-edit/'+this.song._id]);
        },1500);
      },
      error=>{
        console.log(error);
        this.status=false;
        this.message="Error al crear nueva canción";
      }
    )
  }

}
