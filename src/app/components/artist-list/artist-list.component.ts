import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/User';
import { Artist } from 'src/app/models/Artist';
//servicios
import { UserService } from 'src/app/services/user.service';
import { ArtistService } from 'src/app/services/artist.service';
//animaciones 
import {fadeInOut} from '../../../assets/animationsAngular';
//url API
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.css'],
  animations:[fadeInOut],
})
export class ArtistListComponent implements OnInit {

  public title:string;
  public identity:User;
  public token:string;
  public status:boolean;
  public artists:any;
  public page:number;
  public totalPages:number;
  public arrayPages:Array<any>;
  public url:string;
  public message:string;
  constructor(private _userService:UserService,private _artistService:ArtistService) {
    this.title="Listado de Artistas";
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
    this.url=environment.apiURL;
    this.arrayPages=[];
    this.page=0;
   }

  ngOnInit() {
    this.page++;
    this.listPage();
  }

  nextPage(){
    if(this.page<this.totalPages){
      this.page++;
      console.log(this.page);
      this.listPage();
    }
  }

  previousPage(){
    if(this.page>1){
      this.page--;
      console.log(this.page);
      this.listPage();
    }
  }

  clickPageNumber(page){
    this.page=page;
    this.listPage();
  }

  listPage(){
    this._artistService.listArtists(this.page).subscribe(
      response=>{
        console.log(response);
        this.artists=response.result.data;
        this.totalPages=response.result.pagination.totalPages;
        this.arrayPages=[];
        for(let i=1;i<=this.totalPages;i++){
          this.arrayPages.push(i);
        }
      },  
      error=>{
        console.log(error);
      }
    );
  }

  delete(artistId){
    this._artistService.delete(artistId).subscribe(
      response=>{ 
        this.status=true;
        this.message=response.message;
        window.alert(this.message);
        this.listPage();
      },
      error=>{
        this.status=false;
        this.message='Error al borrar el artista';
        window.alert(this.message);
        console.log(error);
      }
    );
  }

}
