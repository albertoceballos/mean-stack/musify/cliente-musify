import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

//animaciones
import { fadeInOut } from 'src/assets/animationsAngular';
import { AlbumService } from 'src/app/services/album.service';

//Modelos
import { Song } from 'src/app/models/Song';
import { Album } from 'src/app/models/Album';
import { Artist } from 'src/app/models/Artist';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  animations: [fadeInOut],
})
export class SearchComponent implements OnInit {

  public pageTitle: string;
  public searchString: string;
  public status: boolean;
  public message: string;
  public artists: Array<Artist>
  public albums: Array<Album>;
  public songs: Array<Song>;
  public url:string;

  constructor(private _activatedRoute: ActivatedRoute, private _router: Router, private _albumService: AlbumService) {
    this.pageTitle = "Búsqueda";
    this.url=environment.apiURL;
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params => {
        this.searchString = params['searchString'];
        console.log('búsqueda', this.searchString);
        this.makeSearch(this.searchString);
      }
    );
  }

  makeSearch(searchString) {
    this._albumService.search(searchString).subscribe(
      response => {
        console.log(response);
        this.status = true;
        //si response.ok es true hubo alguna coincidencia
        if (response.ok) {
          this.artists = response.artists;
          this.albums = response.albums;
          this.songs = response.songs;

          console.log(this.songs);

          var numArtists = this.artists.length;
          var numAlbums = this.albums.length;
          var numSongs = this.songs.length;

          var msgArtist: string;
          numArtists == 1 ? msgArtist = "artista" : msgArtist = "artistas";
          var msgAlbum: string;
          numAlbums == 1 ? msgAlbum = "album" : msgAlbum = "albums";
          var msgSong: string;
          numSongs == 1 ? msgSong = "canción" : msgSong = "canciones";
          this.message = "Encontrados: " + numArtists + " " + msgArtist + ", " + numAlbums + " " + msgAlbum + ", " + numSongs + " " + msgSong;

        } else {
          //si no response.ok es false  entonces no hubo coincidencias en la búsqueda
          this.status = false;
          this.message = response.message;
          this.artists=null;
          this.albums=null;
          this.songs=null;
        }
      },
      error => {
        console.log(error);
        this.status = false;
        this.message = "Error en la petición de búsqueda";
      }
    );
  }

  divPlayEnter(event,id){
    document.getElementById("div-img-song"+id).style.webkitFilter="brightness(50%)";
  }

  divPlayLeave($event,id){
    document.getElementById("div-img-song"+id).style.webkitFilter="brightness(100%)";
  }

  onClickPlay(song){
    console.log("click play", song);
    var file_path=this.url + 'song/get-file/'+song.file;
    var image_path=this.url + 'album/get-image/'+ song.album._id;

    //persistir en localStorage la canción
    localStorage.setItem("song",JSON.stringify(song));

    document.getElementById("player-status").innerHTML="Reproduciendo";
    //nombre de artista del reproductor
    document.getElementById("player-song-title").innerHTML=song.title;
    //nombre del artista en el reproductor
    //document.getElementById("player-artist-name").innerHTML=song.album.artist.name;
    //imagen del album en el reproductor
    document.getElementById("player-album-img").setAttribute('src',image_path);
    //archivo de audio en el reproductor
    document.getElementById("player-source").setAttribute('src', file_path);
    (document.getElementById("player") as any).load();
    (document.getElementById("player") as any).play();
  }

}
