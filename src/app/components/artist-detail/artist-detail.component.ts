import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
//modelos
import { Artist } from 'src/app/models/Artist';
//servicios
import { ArtistService } from 'src/app/services/artist.service';
import { AlbumService } from 'src/app/services/album.service';
import { Album } from 'src/app/models/Album';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.css']
})
export class ArtistDetailComponent implements OnInit {

  public title: string;
  public artist: Artist;
  public url: string;
  public albums: Array<Album>;
  public status: boolean;
  public message: string;
  constructor(private _artistService: ArtistService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _albumService: AlbumService) {
    this.title = "Detalle del artista";
    this.url = environment.apiURL;
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params => {
        var artistId = params['artistId'];
        this._artistService.getArtist(artistId).subscribe(
          response => {
            console.log(response);
            this.artist = response.artist;
            //llamar a extraer albums
            this.getAlbums();
          },
          error => {
            console.log(error);
          }
        );
      }
    );

  }

  //extraer albums del artista
  getAlbums() {
    this._albumService.getAll(this.artist._id).subscribe(
      resp => {
        this.albums = resp.albums;
      },
      err => {
        console.log(err);
      }
    );
  }


  //borrar album
  deleteAlbum(albumId) {
    this._albumService.delete(albumId).subscribe(
      response => {
        this.status = true;
        this.message = response.message;
        alert(this.message);
        this.getAlbums();
      },
      error => {
        console.log(error);
        this.status = false;
        this.message = 'Error al borrar el álbum';
        alert(this.message);
      }
    );
  }

}
