import { Component, OnInit } from '@angular/core';
import {fadeInOut} from '../../../assets/animationsAngular';
import { User } from 'src/app/models/User';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  animations:[fadeInOut],
})
export class RegisterComponent implements OnInit {

  public title:string;
  public user:User;
  public status:boolean;
  public message:string;
  public identity:any;
  constructor(private _userService:UserService) {
    this.title="Regístrate";
    this.user=new User('','ROLE_USER','','','','',null,'');
   }

  ngOnInit() {
  }

  onSubmit(){
    this._userService.register(this.user).subscribe(
      response=>{
        console.log(response);
        this.status=response.ok;
        this.message=response.message;
        if(this.status==true){
          this.identity=response.user;
        }
      },
      error=>{
        console.log(error);
        this.status=false;
        if(error.error.err.code== 11000){
          this.message="Ya existe un usuario con ese E-mail";
        }else{
          this.message=error.error.err.errmsg;
        }
      }
    );
  }

}
