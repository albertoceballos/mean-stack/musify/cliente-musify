import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

//animacion
import { fadeInOut } from '../../../assets/animationsAngular';
//modelos
import { Album } from 'src/app/models/Album';
//servicios
import { AlbumService } from 'src/app/services/album.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  //selector: 'app-album-edit',
  templateUrl: '../album-add/album-add.component.html',
  styleUrls: ['./album-edit.component.css'],
  animations: [fadeInOut],
})
export class AlbumEditComponent implements OnInit {

  public pageTitle: string;
  public album: Album;
  public status: boolean;
  public message: string;
  public is_edit: boolean;
  public url: string;
  public filesToUpload: Array<File>;
  public token: string;
  constructor(private _activatedRoute: ActivatedRoute, private _router: Router, private _albumService: AlbumService, private _userService: UserService) {
    this.pageTitle = "Editar álbum";
    this.is_edit = true;
    this.url = environment.apiURL;
    this.token = this._userService.getToken();
  }

  ngOnInit() {
    //extraer id del álbum de la URL
    this._activatedRoute.params.subscribe(
      params => {
        var albumId = params['albumId'];
        //con el id buscar y extraer todos los datos del álbum
        this._albumService.get(albumId).subscribe(
          response => {
            console.log(response);
            if (!response.album) {
              this._router.navigate(['/']);
            } else {
              this.album = response.album;
            }
          },
          error => {
            console.log(error);
          }
        );
      },
    );
  }

  onSubmit() {
    this._albumService.update(this.album._id, this.album).subscribe(
      response => {
        console.log(response);
        if (this.filesToUpload) {
          this.UploadFileRequest(this.url + 'album/upload-image/' + this.album._id, [], this.filesToUpload).then((result: any) => {
            //mostar nueva imagen
            console.log("Resultado subida imagen: ",result);
            document.getElementById('image').setAttribute('src', this.url + 'album/get-image/' + result.album._id);
          })
          .catch((err)=>{
            console.log(JSON.parse(err));
            var errorMessage=JSON.parse(err);
            this.status=false;
            this.message=errorMessage.message;
            return;
          }); 
        }      
          this.status = true;
          this.message = response.message;
        
      },
      error => {
        console.log(error);
        this.status = false;
        if (error.error.message) {
          this.message = error.error.message;
        } else {
          this.message = "Error al actualizar álbum, inténtalo más tarde";
        }
      }
    );
  }

  onChangeImage(fileData) {
    this.filesToUpload = fileData.target.files;
    console.log(this.filesToUpload);
  }

  //subida de imagen de artista mediante petición Ajax
  UploadFileRequest(url: string, params: Array<any>, files: Array<File>) {
    var token = this.token;
    return new Promise((resolve, reject) => {
      var formData = new FormData();
      var xhr = new XMLHttpRequest();
      for (let i = 0; i < files.length; i++) {
        formData.append('image', files[i], files[i].name);
      }

      xhr.onreadystatechange = function () {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }
      xhr.open('POST', url, true);
      xhr.setRequestHeader('Authorization', token);
      xhr.send(formData);

    });
  }

}
