import { Component, OnInit, ɵConsole } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

//servicios
import { AlbumService } from '../../services/album.service';
import { SongService } from 'src/app/services/song.service';
import { UserService } from 'src/app/services/user.service';
//modelos
import { Album } from '../../models/Album';
import { environment } from 'src/environments/environment';
import { Song } from 'src/app/models/Song';


@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.css']
})
export class AlbumDetailComponent implements OnInit {

  public pageTitle: string;
  public url: string;
  public album: Album;
  public songs: Array<Song>;
  public identity;
  public message:string;
  public status:boolean;
  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _albumService: AlbumService,
    private _songService: SongService,
    private _userService: UserService) {
    this.pageTitle = "Detalle del album";
    this.url = environment.apiURL;
    this.identity=this._userService.getIdentity();
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe(
      params => {
        var albumId = params['albumId'];
        this.getAlbum(albumId);
        this.getSongsAlbum(albumId);
      }
    )
  }

  getAlbum(albumId) {
    this._albumService.get(albumId).subscribe(
      response => {
        this.album = response.album;
      },
      error => {
        console.log(error);
      }
    );
  }

  getSongsAlbum(albumId) {
    this._songService.getAll(albumId).subscribe(
      response => {
        console.log(response);
        this.songs = response.songs;
      },
      error => {
        console.log(error);
      }
    );
  }

  mouseEnter(event) {
    var playBtn = event.target.childNodes[0].childNodes[1];
    playBtn.classList.remove("d-none");
  }

  mouseLeave(event) {
    var playBtn = event.target.childNodes[0].childNodes[1];
    playBtn.classList.add("d-none");
  }


  mostrarBotonesBorrar(event,id){
    console.log(id);
    event.target.classList.add("d-none");
    document.getElementById("botones"+id).classList.remove("d-none");
  }

  clickCancel(event,id){
    document.getElementById("botones"+id).classList.add("d-none");
    document.getElementById("borrarBtn"+id).classList.remove("d-none");
  }

  deleteConfirm(id){
    this._songService.delete(id).subscribe(
      response=>{
        console.log(response);
        this.status=true;
        this.message=response.message;
        alert(this.message);
        this.getSongsAlbum(this.album._id);
      },
      error=>{
        console.log(error);
        this.status=false;
        this.message="Error al borrar mensaje";
        alert(this.message);
      }
    );
  }

  onClickPlay(song){
    console.log("click play", song);
    var file_path=this.url + 'song/get-file/'+song.file;
    var image_path=this.url + 'album/get-image/'+ song.album._id;

    //persistir en localStorage la canción
    localStorage.setItem("song",JSON.stringify(song));

    document.getElementById("player-status").innerHTML="Reproduciendo";
    //nombre de artista del reproductor
    document.getElementById("player-song-title").innerHTML=song.title;
    //nombre del artista en el reproductor
    document.getElementById("player-artist-name").innerHTML=song.album.artist.name;
    //imagen del album en el reproductor
    document.getElementById("player-album-img").setAttribute('src',image_path);
    //archivo de audio en el reproductor
    document.getElementById("player-source").setAttribute('src', file_path);
    (document.getElementById("player") as any).load();
    (document.getElementById("player") as any).play();
  }

}
