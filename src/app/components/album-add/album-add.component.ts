import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

//animacion
import { fadeInOut } from '../../../assets/animationsAngular';
//modelos
import { Album } from 'src/app/models/Album';
//servicios
import { AlbumService } from 'src/app/services/album.service';

@Component({
  selector: 'app-album-add',
  templateUrl: './album-add.component.html',
  styleUrls: ['./album-add.component.css'],
  animations: [fadeInOut],
})
export class AlbumAddComponent implements OnInit {

  public pageTitle: string;
  public album: Album;
  public status: boolean;
  public message: string;
  public is_edit: boolean;

  constructor(private _activatedRoute: ActivatedRoute,private _router:Router, private _albumService: AlbumService) {
    this.pageTitle = 'Crear nuevo álbum';
    this.album = new Album('', '', 1990, null, '', '');
    this.is_edit = false;
  }

  ngOnInit() {
    //extraer id de artista de la ruta y pasarla al objeto artist
    this._activatedRoute.params.subscribe(
      params => {
        var artistId = params['artistId'];
        console.log(artistId);
        this.album.artist = artistId;
      }
    );
  }

  onSubmit() {
    console.log(this.album);
    this._albumService.create(this.album).subscribe(
      response => {
        if (!response.album) {
          this.status = false;
          this.message = 'Error en el servidor';
        } else {
          this.status = true;
          this.message = response.message;
          this.album=response.album;
          setTimeout(() => {
            this._router.navigate(['/album-edit/'+ this.album._id]);
          },1500);
        }

      },
      error => {
        console.log(error);
        this.status = false;
        this.message = 'Error al crear nuevo álbum';
      }
    );
  }
}
