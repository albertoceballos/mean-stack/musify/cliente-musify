import { Component, OnInit } from '@angular/core';
//archivo animaciones
import {fadeInOut} from '../../../assets/animationsAngular';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations:[fadeInOut],
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
